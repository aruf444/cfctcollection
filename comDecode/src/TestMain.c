#include <stdio.h>
#include "include\comDecode.h"

int main(int argc, char *argv[])
// int main(void)
{
	command_t com[] = {{"d%d", 3},
					   {"d%c", 3},
					   {"b##", 3},
					   {"c#", 3},
					   {"aus", 2},
					   {"off", 2},
					   {"ein", 1},
					   {"on", 1},
					   {"", -1}};
	cmd_return_t ret;

	char testStr[40] = "einaus bA b1cAAaus";
	char *ptr;
	char rxByte;
	int ledState = 0;

	if (argc > 1)
	{
		char *argPtr;
		int i, byteCnt;

		ptr = argv[1];
		// printf("1. Arg: %s\n", ptr);
		argPtr = argv[1];
		for (i = 1, byteCnt = 0; i < argc && byteCnt < 40; byteCnt++)
		{
			testStr[byteCnt] = *argPtr;
			argPtr++;
			if (*argPtr == '\0')
			{
				i++;
				argPtr = argv[i];
			}
		}
		testStr[byteCnt] = '\0';
	}
	else
	{
	}
	printf("Teststring: %s\n", testStr);
	ptr = testStr;

	initComDecode();

	for (; *ptr != '\0'; ptr++)
	{
		rxByte = *ptr;

		if (rxByte > 0)
		{
			if (rxByte != ' ')
			{
				printf("%c: ", rxByte);
				ret = decode(com, rxByte);
				printf("%d\n", ret.wert);
				switch (ret.code)
				{
				case 1: // ein
					ledState = 1;
					puts("\nLed ein\n");
					break;
				case 2: // aus
					ledState = 0;
					puts("\nLed aus\n");
					break;
				case 3:
					ledState = 0;
					printf("State: 3, %d\n", ret.wert);
					break;
				case -1:
					puts("\nIch verstehe Dich nicht\n");
					break;
				}
			}
		}
	}
}
