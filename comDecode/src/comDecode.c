#include "include\comDecode.h"
#include "include\comDecodePrvtV2.h"

// #define DBM1
// #define DBM2

#if defined(DBM1) || defined(DBM2)
#include <stdio.h>
#include <conio.h>
char *ctlStTxt[] = {"stCmdCheck", "stBinInput", "stDecInput", "stEnd"};
#endif

#if defined(DBM1)
#define DB1print(...) printf(__VA_ARGS__)
#else
#define DB1print(...)
#endif

#if defined(DBM2)
#define DB2print(...) printf(__VA_ARGS__)
#else
#define DB2print(...)
#endif

// private Variablen
char buffer[BUFFERLEN];
char *currBuffPtr = BUFFER_START;
char *currBuffEndPtr = BUFFER_START;
int comCnt = 0;
int pos = 0;
int retVal = 0;
int retValSign = 1;

// typedef enum {stCmdCheck, stBinInput, stDecInput, stEnd} cntrlStateTyp;
cntrlStateTyp cntrlState = stCmdCheck;

void initComDecode(void)
{
  currBuffPtr = BUFFER_START;
  currBuffEndPtr = BUFFER_START;
  comCnt = 0;
  pos = 0;
  retVal = 0;
  retValSign = 1;
  cntrlState = stCmdCheck;
}

cmd_return_t decode(command_t com_array[], char inputChar)
{
  char currBuffSym, cmdSym;
  cmd_return_t ret;
  ret.wert = 0;

  // unsigned char ctlFlgNxtBuffSymReq = 0, ctlFlgNxtCmdSymReq = 0;
  // unsigned char ctlFlgRstBuffPtr = 0, ctlFlgNxtCmd = 0;
  unsigned char loopExit = 0;

  // neues Zeichen dem Buffer anf�gen
  if (currBuffEndPtr != BUFFER_END)
  {
    *currBuffEndPtr = inputChar;
    currBuffPtr = currBuffEndPtr;
    currBuffEndPtr++;
    // currBuffEndPtr = currBuffPtr;
    //  zur Fehlersuche:
    *currBuffEndPtr = '\0'; // nur f�r Debug-Zweck erforderlich
  }
  else // Buffer voll ==> Reset + Fehler
  {
    initComDecode();
    ret.code = -1;
  }

  // cmdSym = com_array[comCnt].command[pos];
  DB1print("BUFF: %s ", buffer);

  cmdSym = com_array[comCnt].command[pos];
  while (!loopExit)
  {
    currBuffSym = *(currBuffPtr);
    // DB2print("State: %d;%c;%c| ", (int)cntrlState, cmdSym, currBuffSym);
    DB2print("State: %s;%c;%c;%d| ", ctlStTxt[(int)cntrlState], cmdSym, currBuffSym, retVal);

    switch (cntrlState)
    {
    case stCmdCheck:
      if ('#' == cmdSym) // Bin�rer Input
      {
        cntrlState = stBinInput;
        // ctlFlgNxtBuffSymReq = 0;
        // ctlFlgNxtCmdSymReq = 0;
        // ctlFlgRstBuffPtr = 0;
        // ctlFlgNxtCmd = 0;
        retVal = 0;
      }
      else if ('%' == cmdSym) // Dezimalzahl
      {
        cntrlState = stDecInput;
        // ctlFlgNxtBuffSymReq = 0;
        // ctlFlgNxtCmdSymReq = 0;
        // ctlFlgRstBuffPtr = 0;
        // ctlFlgNxtCmd = 0;
        retVal = 0;
      }
      else if (currBuffSym == cmdSym) //
      {
        cntrlState = stCmdCheck;
        currBuffPtr++; // ctlFlgNxtBuffSymReq = 1;
        pos++;         // ctlFlgNxtCmdSymReq = 1;
        // ctlFlgRstBuffPtr = 0;
        // ctlFlgNxtCmd = 0;
      }
      else // Inputzeichen passt nicht zum Kommando
      {
        cntrlState = stCmdCheck;
        // ctlFlgNxtBuffSymReq = 0;
        // ctlFlgNxtCmdSymReq = 0;
        currBuffPtr = BUFFER_START; // ctlFlgRstBuffPtr = 1;
        comCnt++;                   // ctlFlgNxtCmd = 1;
        pos = 0;
      }
      break;

    case stBinInput:
      if ('#' == cmdSym) // Bin�rer Input
      {
        cntrlState = stBinInput;
        currBuffPtr++; // ctlFlgNxtBuffSymReq = 1;
        pos++;         // ctlFlgNxtCmdSymReq = 1;
        // ctlFlgRstBuffPtr = 0;
        // ctlFlgNxtCmd = 0;
        retVal = (retVal << 8) + (int)currBuffSym;
      }
      else
      {
        cntrlState = stCmdCheck;
        // ctlFlgNxtBuffSymReq = 0;
        // ctlFlgNxtCmdSymReq = 0;
        // ctlFlgRstBuffPtr = 0;
        // ctlFlgNxtCmd = 0;
      }
      break;
    case stDecInput:
      if ('0' <= currBuffSym && '9' >= currBuffSym)
      {
        cntrlState = stDecInput;
        currBuffPtr++; // ctlFlgNxtBuffSymReq = 1;
        // ctlFlgNxtCmdSymReq = 0;
        // ctlFlgRstBuffPtr = 0;
        // ctlFlgNxtCmd = 0;
        retVal = retVal * 10 + (currBuffSym - '0');
      }
      else
      {
        cntrlState = stCmdCheck;
        // ctlFlgNxtBuffSymReq = 0;
        pos++; // ctlFlgNxtCmdSymReq = 1;
        // ctlFlgRstBuffPtr = 0;
        // ctlFlgNxtCmd = 0;
      }
    }
    // Ende Switch-Case

    /// Auswertung der Control-Flags
    /* if (ctlFlgNxtBuffSymReq)
    {
      currBuffPtr++;
    }
    if (ctlFlgRstBuffPtr)
    {
      currBuffPtr = BUFFER_START;
    }

    if (ctlFlgNxtCmdSymReq)
    {
      pos++;
    }
    else
    {
      ;
    }
    if (ctlFlgNxtCmd)
    {
      comCnt++;
      pos = 0;
    }
    else
    {
      ;
    } */

    cmdSym = com_array[comCnt].command[pos];

    if ('\0' == cmdSym) // Befehl gefunden
    {
      ret.code = com_array[comCnt].code;
      ret.wert = retVal;
      initComDecode();
      // _getch();
      // currBuffEndPtr = BUFFER_START;
      loopExit = 1;
    }
    // Anderes Kriterium besser?
    else if (currBuffPtr == currBuffEndPtr) // Pufferende erreicht, auf weitere Zeichen warten
    {
      ret.code = 0;
      loopExit = 1;
    }
  }
  return ret;
}
