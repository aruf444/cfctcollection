#if !defined(BUFFERLEN)
#define BUFFERLEN 16
#endif

#define BUFFER_START &buffer[0]
#define BUFFER_END &buffer[BUFFERLEN - 1]

typedef enum
{
    stCmdCheck,
    stBinInput,
    stDecInput
} cntrlStateTyp;
