#if !defined(DECODE)
typedef struct
{ char const *command;
  char code;
  
} command_t;

typedef struct
{ int code;
  int wert;
} cmd_return_t;
#endif

cmd_return_t decode(command_t com_array[], char c);
void initComDecode(void);
//int decode(command_t com_array[], buffer_t *buff);

#define DECODE
