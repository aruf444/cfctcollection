/****************************************************************************
 *                                                                          *
 * File    : main.c                                                         *
 *                                                                          *
 * Purpose : Console mode (command line) program.                           *
 *                                                                          *
 * History : Date      Reason                                               *
 *           00/00/00  Created                                              *
 *                                                                          *
 ****************************************************************************/

#include <stdio.h>
#include "comDecode.h"

/****************************************************************************
 *                                                                          *
 * Function: main                                                           *
 *                                                                          *
 * Purpose : Main entry point.                                              *
 *                                                                          *
 * History : Date      Reason                                               *
 *           00/00/00  Created                                              *
 *                                                                          *
 ****************************************************************************/



int main(int argc, char *argv[])
{
	command_t com[] = { { "ein", 1},
		                { "aus", 2},
		                { "", 0}};
	cmd_return_t ret;

	char testStr[] = "ein aus eiaus";
	char *ptr = testStr;
	char rxByte;
	int ledState = 0;

	initComDecode();

	for (; *ptr != '\0'; ptr++)
	{
		rxByte = *ptr;

		if (rxByte > 0)
		{
			if (rxByte != ' ')
			{
				printf("%c: ", rxByte);
				ret = decode(com, rxByte);
				switch (ret.code)
				{
					case 1:	// ein
						ledState = 1;
						puts("\nLed ein\n");
						break;
					case 2:	// aus
						ledState = 0;
						puts("\nLed aus\n");
						break;
					case -1:
						puts("\nIch verstehe Dich nicht\n");
						break;
				}
			}
		}
	}
}

