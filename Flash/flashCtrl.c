#include <stdio.h>

#include "flashCtrl.h"

void initFlash(tFlashItem *flshItem, char unsigned currentTime, char unsigned nOnIn, char unsigned nPeriodIn, char unsigned nRepeatIn)
{  flshItem->startTime = currentTime;
	flshItem->nRepeat = nRepeatIn;
	flshItem->ledState = OFF;
	flshItem->nOn = nOnIn;
	flshItem->nPeriod = nPeriodIn;
	flshItem->state = ST_FLASH;
   printf("Init:  state: %d, nOn: %d, nPrd: %d, nRpt: %d\n", flshItem->state, flshItem->nOn, flshItem->nPeriod, flshItem->nRepeat);
}

void initPatternFlash(tFlashItem *flshItem, char unsigned currentTime, short unsigned patternIn, char unsigned length, char unsigned nRepeatIn)
{  flshItem->startTime = currentTime;
	flshItem->nRepeat = nRepeatIn;
	flshItem->ledState = OFF;
	flshItem->pattern = patternIn & ((1 << length) - 1);
	flshItem->pattern |= (1 << length);
	flshItem->workPattern = flshItem->pattern;
   flshItem->state = ST_PATTERNFLASH;
   printf("Init:  state: %d, pattern: %x, nRpt: %d\n", flshItem->state, flshItem->pattern, flshItem->nRepeat);
}

void ledOn(tFlashItem *flshItem)
{ flshItem->state = ST_ON;
}

void ledOff(tFlashItem *flshItem)
{ flshItem->state = ST_OFF;
}

void updateFlash(tFlashItem *flshItem, char unsigned currentTime)
{ char unsigned deltaTime;
   
  switch (flshItem->state)
  { case ST_ON:
	   flshItem->ledState = ON;
	   break;

    case ST_FLASH:
     deltaTime = (currentTime-flshItem->startTime);
     while (deltaTime >= flshItem->nPeriod)
	  { flshItem->startTime += flshItem->nPeriod;
	    deltaTime -= flshItem->nPeriod;
		 if (flshItem->nRepeat < 255)
       { flshItem->nRepeat--;
  	      if (flshItem->nRepeat == 0)
			{ flshItem->state = ST_OFF;
		   }
	    } 
     }
	  flshItem->ledState = (tLedSt) (deltaTime < flshItem->nOn && flshItem->state != ST_OFF);
	  break;

   case ST_PATTERNFLASH:
     deltaTime = (currentTime-flshItem->startTime);
	  while (deltaTime > 0 && flshItem->nRepeat > 0)
	  { flshItem->workPattern = flshItem->workPattern >> 1;
       flshItem->startTime++;
		 deltaTime--;
		 if (flshItem->workPattern == 1)
       { if (flshItem->nRepeat < 255)
		   { flshItem->nRepeat--;
			}
			flshItem->workPattern = flshItem->pattern;
		 }
	  }
	  if (flshItem->nRepeat == 0)
	  { flshItem->state = ST_OFF;
		 flshItem->ledState = OFF;
	  }
	  else 
	  { flshItem->ledState = (tLedSt) flshItem->workPattern & 0x01;
	  }
	  printf("wp: 0x%x, nRep: %d || ", flshItem->workPattern, flshItem->nRepeat);
	  break;

   default:
	  flshItem->ledState = OFF;
	  break;
  }
}

char getFlashState(tFlashItem *flshItem)
{ return flshItem->ledState;
}

 
