/****************************************************************************
 *                                                                          *
 * File    : main.c                                                         *
 *                                                                          *
 * Purpose : Console mode (command line) program.                           *
 *                                                                          *
 * History : Date      Reason                                               *
 *           00/00/00  Created                                              *
 *                                                                          *
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "flashCtrl.h"

int main(int argc, char *argv[])
{   double time = 0.0;
	char unsigned cntr125us;
	char unsigned stateFlashInit = 0;
	tFlashItem led1;


	while (time < 0.0095)
    {  cntr125us = (char unsigned) (((int unsigned)(time*8000.0)) % 256); 
       //------
	   if (time >= 0.0005 && stateFlashInit == 0)
       { initFlash(&led1, cntr125us, 3, 8, 4);
		 stateFlashInit = 1;
       }
	   else if (time >= 0.005 && stateFlashInit == 1)
	   { initPatternFlash(&led1, cntr125us, 0x13, 7, 2);
	     stateFlashInit = 2;
	   }
	   else if (time > 0.008 && stateFlashInit == 2)
	   { ledOn(&led1);
	     stateFlashInit = 3;
       }
	   else if (time > 0.009 && stateFlashInit == 3)
	   { ledOff(&led1);
	     stateFlashInit = 4;
       }
       //------
       updateFlash(&led1, cntr125us);
       //------ 
       printf("Zeit %6.3f ms (%5.2f); Counter: %3d; LED: %d\n", time*1000, time*8000, cntr125us, (int) getFlashState(&led1)); 
	   time += ((rand() % 50) + 70)*1e-6;
	}
    
    return 0;
}

