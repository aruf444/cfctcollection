#ifndef FLASHCTRL_HEADER
#define FLASHCTRL_HEADER

typedef enum {OFF = 0, ON} tLedSt;
typedef enum {ST_OFF, ST_ON, ST_FLASH, ST_PATTERNFLASH} tFlshSt;

typedef struct 
{ char unsigned startTime;
  char unsigned nRepeat;
  tLedSt ledState;
  tFlshSt state;
  union 
  { struct 
    { char unsigned nOn;
      char unsigned nPeriod;
    };
    struct 
    { short unsigned pattern;
      short unsigned workPattern;
    };
  };
} tFlashItem; 


void initFlash(tFlashItem *flshItem, char unsigned currentTime, char unsigned nOnIn, char unsigned nPeriodIn, char unsigned nRepeatIn);
void initPatternFlash(tFlashItem *flshItem, char unsigned currentTime, short unsigned patternIn, char unsigned length, char unsigned nRepeatIn);

void updateFlash(tFlashItem *flshItem, char unsigned currentTime);

void ledOn(tFlashItem *flshItem);
void ledOff(tFlashItem *flshItem);

char getFlashState(tFlashItem *flshItem);

#endif
